from pymongo import MongoClient, errors
from secrets import server2
import re

count = 0
maxSevSelDelay = 3000

print ("\n\nUpdate script Started")

try:
	con2 = MongoClient('mongodb://' + server2['username'] +':' + server2['password'] + "@" + server2['ip'] + ':' + server2['port'] + '/?authSource=' + server2['authSource'], serverSelectionTimeoutMS = maxSevSelDelay )

	con2.server_info()

except errors.ServerSelectionTimeoutError, e:
	print("Server not found: %s" %e)

else:
	db2 = con2[server2['db']]

	col2 = db2[server2['collection']]

	try:
		updateResult = col2.find({},{"id": 1, "contents": 1, "_id": 0}, no_cursor_timeout = True)

		print("Records found: %d" %updateResult.count() )

	except e:
		print("Error occured in finding Records: %s" %e )

	else:
		for a in updateResult:

			id = a["id"]
			str = re.sub(r'[\t|\n]+', ' ', a["contents"])

			col2.update({"id": id}, { "$set": {"contents": str }} )
			count += 1

		print("Records Updated: %d" %count)

	finally:
		updateResult.close()

print("Completed...")
