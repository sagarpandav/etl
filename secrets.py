#Read from
server1 = {
	'ip' : '192.168.33.12',
	'port' : '27017',
	'username' : 'rAdmin',
	'password' : 'pass',
	'db' : 'hire_scikey',
	'collection' : 'resumes',
	'authSource' : 'admin'
}

#Write to
server2 = {
	'ip' : '192.168.33.13',
	'port' : '27017',
	'username' : 'rwAdmin',
	'password' : 'pass',
	'db' : 'hire_scikey',
	'collection' : 'resumes',
	'authSource' : 'admin'
}

#Reschedual time
time = {
	'hours' : 4
}

#Required Attributes
projection_attr = {
	"_id" : 0,
	"created_on" : 1,
	"created_by" : 1,
	"updated_on" : 1,
	"updated_by" : 1,
	"contents" : 1,
	"candidate_id" : 1,
	"is_active" : 1,
	"is_deleted" : 1,
	"id" : 1
}

