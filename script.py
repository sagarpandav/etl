from pymongo import MongoClient, errors
from datetime import datetime, timedelta
from secrets import server1, server2, time, projection_attr
import re

count = 0
maxSevSelDelay = 3000

print "\n\nETL script Started at ",datetime.now()

try:
	con1 = MongoClient('mongodb://' + server1['username'] +':' + server1['password'] + "@" + server1['ip'] + ':' + server1['port'] + '/?authSource=' + server1['authSource'], serverSelectionTimeoutMS = maxSevSelDelay )
	con2 = MongoClient('mongodb://' + server2['username'] +':' + server2['password'] + "@" + server2['ip'] + ':' + server2['port'] + '/?authSource=' + server2['authSource'], serverSelectionTimeoutMS = maxSevSelDelay )

	con1.server_info()
	con2.server_info()

except errors.ServerSelectionTimeoutError, e:
	print("Server not found: %s" %e)

else:
	db1 = con1[server1['db']]
	db2 = con2[server2['db']]

	col1 = db1[server1['collection']]
	col2 = db2[server2['collection']]

	try:
		updateResult = col1.find({"updated_on" : { "$gte": datetime.now() - timedelta( hours = time['hours'] ) } }, projection_attr, no_cursor_timeout = True)

		print("Records found: %d" %updateResult.count() )

	except e:
		print("Error occured in finding Records: %s" %e )

	else:
		for a in updateResult:

			id = a["id"]
			a["contents"] = re.sub(r'[\t|\n]+', ' ', a["contents"])

			col2.update({"id": id}, a, upsert = True )
			count += 1

		print("Records Updated: %d" %count)

	finally:
		updateResult.close()

	try:
		deleteResult = col2.delete_many({"$or":[{ "is_deleted": True }, { "is_active": False }] })
		print("Records deleted: %d" %deleteResult.deleted_count)

	except e:
		print("Error occured in deleting Records: %s" %e)

print "Completed..."
